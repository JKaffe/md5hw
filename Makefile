md5model: model/BlockFeeder.cpp model/main.cpp model/Md5Model.cpp model/RoundProcessor.cpp
	g++ -Imodel/ model/BlockFeeder.cpp model/main.cpp model/Md5Model.cpp model/RoundProcessor.cpp -o md5model

.PHONY:test-rotator
test-rotator: obj_dir/VRotateLeftUnit
	./obj_dir/VRotateLeftUnit

obj_dir/VRotateLeftUnit: tb/tb_RotateLeftUnit.cpp
	verilator -Wall --trace --cc RotateLeftUnit.v --exe tb/tb_RotateLeftUnit.cpp -Isrc/ -LDFLAGS -lpthread
	make -C obj_dir -f VRotateLeftUnit.mk VRotateLeftUnit

.PHONY:test-processor
test-processor: obj_dir/Vtb_RoundProcessor
	./obj_dir/Vtb_RoundProcessor

obj_dir/Vtb_RoundProcessor:
	verilator -Wall --trace --cc tb/tb_RoundProcessor.v --exe tb/tb_RoundProcessor.cpp -Isrc/
	make -C obj_dir -f Vtb_RoundProcessor.mk Vtb_RoundProcessor

.PHONY:test-feeder
test-feeder: obj_dir/VBlockFeeder
	./obj_dir/VBlockFeeder

obj_dir/VBlockFeeder:
	verilator -Wall --trace --cc src/BlockFeeder.v --exe tb/tb_BlockFeeder.cpp model/BlockFeeder.cpp -Isrc/ -CFLAGS "-g -I../model/"
	make -C obj_dir -f VBlockFeeder.mk VBlockFeeder

.PHONY:test-md5hw
test-md5hw: obj_dir/VMd5Hw
	./obj_dir/VMd5Hw

obj_dir/VMd5Hw:
	verilator -Wall --trace --cc src/Md5Hw.v --exe tb/tb_Md5Hw.cpp -Isrc/
	make -C obj_dir -f VMd5Hw.mk VMd5Hw

.PHONY:clean
clean:
	rm md5model || true
	rm -r obj_dir || true
	rm tb_*_wave.vcd || true
