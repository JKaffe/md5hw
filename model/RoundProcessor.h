#pragma once

#include <cstdint>

#include "types.h"

namespace md5model {
	enum Function {
		F, G, H, I
	};

	class RoundProcessor {
	private:
		reg_t A, B, C, D;
		reg_t Aold, Bold, Cold, Dold;

	public:
		void reset();

		void getRegisters(reg_t& A, reg_t& B, reg_t& C, reg_t& D);

		// Start processing on the current block.
		void startBlock();

		void execOp(enum Function function,
					reg_t constantValue,
					reg_t dataWord,
					std::uint8_t rotate);

		// Terminate the processing of the current block.
		void endBlock();

		// Get the md5 hash.
		void getDigest(digest_t& digest);

	private:
		reg_t evalFunction(enum Function function) {
			const reg_t Dn = ~D;

			const reg_t D_and_Bn = D & ~B;
			const reg_t B_xor_D = B & Dn | D_and_Bn;

			const reg_t Qf = B & C | D_and_Bn;
			const reg_t Qg = B & D | C & Dn;
			const reg_t Qh = B_xor_D ^ C;
			const reg_t Qi = C ^ (~D_and_Bn);

			// Multiplexer.
			switch (function) {
				case F:
					return Qf;
				case G:
					return Qg;
				case H:
					return Qh;
				case I:
					return Qi;
			};

			// Unreachable.
			return ~0;
		}

	};
}
