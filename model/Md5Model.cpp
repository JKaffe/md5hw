#include <cstdint>
#include <cstdio>
#include <fstream>

#include "BlockFeeder.h"
#include "Md5Consts.h"
#include "Md5Model.h"
#include "RoundProcessor.h"

namespace md5model {
	void hash(std::istream& inStream, digest_t& digest) {
		// The counter only needs to count up till 63, so we don't need all 8
		// bits.
		// 4 rounds of 16 op each.
		// +--------------------+
		// | 7 6 5 4	3 2 1 0 |
		// | X C R R	O O O O |
		// +--------------------+
		// X = Unused.
		// C = Carry out from the counter.
		// R = Round count bits.
		// O = Op count bits.
		std::uint8_t opCount = 0U;

		BlockFeeder blockFeeder(inStream);
		RoundProcessor roundProcessor;

		roundProcessor.reset();

		while(blockFeeder.hasNext()) {
			blockFeeder.next();

			opCount = 0U;

			roundProcessor.startBlock();

			// While no carry out.
			while (!(opCount >> 6)) {
				// Normally you would just select bits [5-4].
				const enum Function functionSelector =
					static_cast<enum Function>(opCount >> 4U);

				// Get the constant to use for this op in this round.
				const std::uint32_t constantValue = constValues[opCount];

				const block32_t *block = to_block32_ptr(blockFeeder.getBlock());
				const std::uint32_t *words = *block;

				std::uint8_t messageIdx = perRoundIndecies[opCount];
				std::uint32_t word = words[messageIdx];

				/* Get the value to rotate by. Indexing by the concatenation of
				 * round bits with 2 lsb from op.
				 * The ~0x3U mask is used to make sure that this still works
				 * if >> 4 << 2 gets optimised to just >> 2.
				 */
				const std::uint8_t rotateBy =
					rotateByValues[(opCount >> 4U) << 2U & ~0x3U | opCount & 0x3U];

				roundProcessor.execOp(functionSelector,
									  constantValue,
									  word,
									  rotateBy);

				opCount++;
			}

			roundProcessor.endBlock();
		}

		roundProcessor.getDigest(digest);
	}

	void print(digest_t& digest) {
		for (std::uint32_t i = 0; i < sizeof(digest) / sizeof(digest[0]); i++) {
			std::printf("%08x", digest[i]);
		}
		std::printf("\n");
	}
};
