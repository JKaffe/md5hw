#include <cstdint>
#include <cstring>
#include <fstream>

#include "BlockFeeder.h"
#include "types.h"

using namespace md5model;

BlockFeeder::BlockFeeder(std::istream& inStream) : inStream(inStream) {
	messageLength = 0U;
	state = INIT;
}

const block8_t *BlockFeeder::getBlock() {
	return &block;
}

bool BlockFeeder::hasNext() {
	return state != END;
}

BlockFeeder *BlockFeeder::next() {
	switch (state) {
		case INIT:
			inStream.read(reinterpret_cast<char*>(block), sizeof(block));
			messageLength += inStream.gcount() * 8;

			if (inStream.eof()) {
				uint32_t writtenChars = inStream.gcount();

				if (writtenChars <= (448U / 8U)) {
					// Add padding.
					block[writtenChars] = 0x80U;
					memset(&block[writtenChars + 1U],
						   0U,
						   (448U / 8U) - writtenChars);

					// Write the message length to the last 64 bits.
					memcpy(&block[448 / 8], &messageLength, 8);

					state = END;
				} else {
					// We need another block after this since there is no
					// avaiable space for the message length to be added.

					if (writtenChars == (512U / 8U)) {
						state = ONE_MORE_NEW_PADDING;
					} else {
						block[writtenChars] = 0x80U;
						memset(&block[writtenChars + 1U],
							   0U,
							   (512U / 8U) - writtenChars);

						state = ONE_MORE_OLD_PADDING;
					}
				}
			}
			break;

		case ONE_MORE_NEW_PADDING:
			block[0] = 0x80U;

			memset(&block[1], 0U, (448U / 8U) - 1);

			// Write the message length to the last 64 bits.
			memcpy(&block[448 / 8], &messageLength, 8);

			state = END;
			break;

		case ONE_MORE_OLD_PADDING:
			memset(&block[0], 0U, 448U / 8U);

			// Write the message length to the last 64 bits.
			memcpy(&block[448 / 8], &messageLength, 8);

			state = END;
			break;

	};

	return this;
}
