#include <cstdint>
#include <cstring>

#include "RoundProcessor.h"

using namespace md5model;

void RoundProcessor::reset() {
	A = 0x67452301U;
	B = 0xEFCDAB89U;
	C = 0x98BADCFEU;
	D = 0x10325476U;
}

void RoundProcessor::getRegisters(reg_t& A, reg_t& B, reg_t& C, reg_t& D) {
	A = this->A;
	B = this->B;
	C = this->C;
	D = this->D;
}

void RoundProcessor::startBlock() {
	Aold = A;
	Bold = B;
	Cold = C;
	Dold = D;
}

/* Lets not make this cycle accurate and simulate flip flops to make things
 * easier.
 */
void RoundProcessor::execOp(enum Function function,
							std::uint32_t constantValue,
							std::uint32_t dataWord,
							std::uint8_t rotate) {
	const std::uint32_t Qf = evalFunction(function);

	std::uint32_t Bnext = A + Qf + dataWord + constantValue;
	Bnext = Bnext << rotate | Bnext >> (32U - rotate);
	Bnext = Bnext + B;

	A = D;
	D = C;
	C = B;
	B = Bnext;
}

void RoundProcessor::endBlock() {
	A += Aold;
	B += Bold;
	C += Cold;
	D += Dold;
}

static std::uint32_t byteswap(std::uint32_t val) {
	return (val << 24U) |
		   ((val << 8U) & 0x00FF0000) |
		   ((val >> 8U) & 0x0000FF00) |
		   (val >> 24U);
}

void RoundProcessor::getDigest(digest_t& digest) {
	digest[0] = byteswap(A);
	digest[1] = byteswap(B);
	digest[2] = byteswap(C);
	digest[3] = byteswap(D);
}
