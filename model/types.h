#pragma once

#define MD5_REG_COUNT 4U
#define MD5_BLOCK_SIZE (512U / 8U)

namespace md5model {
	typedef std::uint32_t reg_t;

	typedef std::uint32_t digest_t[MD5_REG_COUNT];

	typedef std::uint8_t block8_t[MD5_BLOCK_SIZE];
	typedef std::uint32_t block32_t[MD5_BLOCK_SIZE / sizeof(std::uint32_t)];

	static const block32_t *to_block32_ptr(const block8_t *ptr) {
		return reinterpret_cast<const block32_t *>(ptr);
	};
}
