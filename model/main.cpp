/* Get Md5 hash on the shell for stdin data using an MD5 hw model.
 * Copyright (C) 2022 Karmjit Mahil
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>

#include "Md5Model.h"

int main() {
	md5model::digest_t digest;
	md5model::hash(std::cin, digest);
	md5model::print(digest);

	return 0;
}
