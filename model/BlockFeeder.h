#pragma once

#include <cstdint>
#include <fstream>

#include "types.h"

namespace md5model {
	class BlockFeeder {
	private:
		enum BlockFeederState {
			INIT,
			ONE_MORE_NEW_PADDING,
			ONE_MORE_OLD_PADDING,
			END
		} state;

		std::istream& inStream;

		std::uint64_t messageLength;

		bool hasNextBlock = true;
		md5model::block8_t block;

	public:
		BlockFeeder(std::istream& inStream);

		const md5model::block8_t *getBlock();

		// Indicates whether there will be another block available.
		bool hasNext();

		BlockFeeder *next();
	};
}
