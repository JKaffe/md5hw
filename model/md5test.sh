# Test script for the md5 model in the md5hw project.
# Copyright (C) 2022 Karmjit Mahil
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

printf "" > a
[ "d41d8cd98f00b204e9800998ecf8427e" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "a" > a
[ "0cc175b9c0f1b6a831c399e269772661" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "abc" > a
[ "900150983cd24fb0d6963f7d28e17f72" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "message digest" > a
[ "f96b697d7cb7938d525a2f31aaf161d0" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "abcdefghijklmnopqrstuvwxyz" > a
[ "c3fcd3d76192e4007dfb496cca67e13b" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" > a
[ "d174ab98d277d9f5a5611c2c9f419d9f" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

printf "12345678901234567890123456789012345678901234567890123456789012345678901234567890" > a
[ "57edf4a22be3c955ac49da2e2107b67a" = "$(./a.out)" ] && printf "PASS\n" || printf "FAIL\n"

