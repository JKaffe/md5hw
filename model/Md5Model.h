#pragma once

#include <cstdint>
#include <fstream>

#include "types.h"

namespace md5model {
	void hash(std::istream& inStream, digest_t& digest);

	void print(digest_t& digest);
};
