/* Test-bench for the RotateLeftUnit module in the md5hw project.
 * Copyright (C) 2022 Karmjit Mahil
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <future>
#include <thread>

#include "VRotateLeftUnit.h"

int simBlock(uint32_t startingValue, uint32_t endingValue) {
	assert(startingValue < endingValue);

	VRotateLeftUnit *dut = new VRotateLeftUnit();

	printf("Checking. Region: 0x%08X - 0x%08X.\n", startingValue, endingValue);

	for (uint64_t i = startingValue; i <= endingValue; i++) {
		dut->D = i;

		for (uint8_t j = 0U; j < 32U; j++) {
			dut->S = j;

			dut->eval();

			const uint32_t expected = i << j | i >> (32U - j);
			const uint32_t observed = dut->Q;

			if (observed != expected) {
				printf("Error. Expected: 0x%08X, Observed: 0x%08X. D: 0x%08X, S: %d.\n",
				       expected, observed, i, j);
				return 1;
			}
		}
	}

	printf("Success. Region: 0x%08X - 0x%08X.\n", startingValue, endingValue);

	delete dut;

	return 0;
}

int main() {
	VRotateLeftUnit *dut = new VRotateLeftUnit();

	const auto threadCount = std::thread::hardware_concurrency();

	std::future<int> future[threadCount];

	for (uint32_t i = 0U; i < threadCount - 1; i++) {
		future[i] = std::async(&simBlock,
							   UINT32_MAX / threadCount * i,
							   UINT32_MAX / threadCount * (i + 1));
	}

	future[threadCount - 1] =
		std::async(&simBlock,
					UINT32_MAX / threadCount * (threadCount - 1),
					UINT32_MAX);

	for (uint32_t i = 0U; i < 8U; i++) {
		if(future[i].get() != 0) {
			printf("Failure!\n");
			return 1;
		}
	}

	printf("Success!\n");

	return 0;
}
