/* Test-bench for the Md5Hw module in the md5hw project.
 * Copyright (C) 2022 Karmjit Mahil
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <verilated.h>
#include <verilated_vcd_c.h>

#include "VMd5Hw.h"

int main() {
	VMd5Hw *dut = new VMd5Hw();
	vluint64_t time = 0;

	Verilated::traceEverOn(true);
	VerilatedVcdC *vcd = new VerilatedVcdC;
	dut->trace(vcd, 5);

	vcd->open("tb_Md5Hw.vcd");

	dut->Clk = 1;
	dut->Rst = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Rst = 0;

	dut->Wdata = ('a' >> 7) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;


	dut->Wdata = ('a' >> 6) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 5) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 4) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 3) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 2) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 1) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wdata = ('a' >> 0) & 0x1;
	dut->Wvalid = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wvalid = 0;

	dut->Wend = 1;
	dut->Trig = 1;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Clk ^= 1;
	dut->eval();
	vcd->dump(time);
	time++;

	dut->Wend = 0;
	dut->Trig = 0;

	for (; time <= 2049; time++) {
		dut->Clk ^= 1;
		dut->eval();
		vcd->dump(time);
		time++;
	}

	vcd->close();
	delete vcd;
	delete dut;

	return 0;
}
