/* Test-bench for the BlockFeeder module in the md5hw project.
 * Copyright (C) 2022 Karmjit Mahil
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>
#include <verilated.h>
#include <verilated_vcd_c.h>

#include "BlockFeeder.h"
#include "VBlockFeeder.h"

#define WORD_SIZE 32U

#define REG_COUNT 4U
#define REG_SIZE WORD_SIZE

#define ARRAY_SIZE(arr) sizeof(arr) / sizeof (*(arr))

static const std::string testDataSet[] = {
	"",
	"a",
	"abc",
	"message digest",
	"abcdefghijklmnopqrstuvwxyz",
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
	"12345678901234567890123456789012345678901234567890123456789012345678901234567890",
};

class TimeOutError: public std::runtime_error {
public:
	TimeOutError(int line_num, const std::string& msg)
		: std::runtime_error(
				std::string("Timeout (L") + std::to_string(line_num) + std::string("): ") + msg) {}
};

#define WAIT(sig, val, timeout, msg)                                 \
	const int line_num = __LINE__;                                   \
	const __typeof((val)) _val = (val);                              \
	for (uint32_t i = 0U; i < timeout && (sig) != _val; i++) {       \
		pulse();                                                     \
	}                                                                \
	if ((sig) != _val) throw TimeOutError(line_num, std::string(msg))

#define MAX_TAG_SIZE 20U

class ValueMismatchError: public std::runtime_error {
private:
	char err_buff[63 + MAX_TAG_SIZE];

	static const char *genMsg(char (*err_buff_ptr)[63 + MAX_TAG_SIZE],
							  int line_num,
							  const char *tag,
							  uint32_t expected,
							  uint32_t observed) {
		snprintf(*err_buff_ptr,
				 sizeof(*err_buff_ptr),
				 "Error (L%d): Expected: 0x%08X. Observed: 0x%08X. - %s.\n",
				 line_num,
				 expected,
				 observed,
				 tag);

		return *err_buff_ptr;
	}

	static const char *genMsg(char (*err_buff_ptr)[63 + MAX_TAG_SIZE],
							  int line_num,
							  const char *tag,
							  bool expected,
							  bool observed) {
		snprintf(*err_buff_ptr,
				 sizeof(*err_buff_ptr),
				 "Error (L%d): Expected: %s. Observed: %s. - %s.\n",
				 line_num,
				 expected ? "true" : "false",
				 observed ? "true" : "false",
				 tag);

		return *err_buff_ptr;
	}

public:
	ValueMismatchError(int line_num, const char *tag, uint32_t expected, uint32_t observed)
		: std::runtime_error(genMsg(&err_buff, line_num, tag, expected, observed)) {}

	ValueMismatchError(int line_num, const char *tag, bool expected, bool observed)
		: std::runtime_error(genMsg(&err_buff, line_num, tag, expected, observed)) {}
};

#undef MAX_TAG_SIZE

#define ASSERT(sig, val) if ((sig) != (val)) throw ValueMismatchError(__LINE__, #sig, (val), (sig))
#define ASSERT2(sig, val, msg) if ((sig) != (val)) throw ValueMismatchError(__LINE__, (msg), (val), (sig))

class TbBlockFeeder: public VBlockFeeder {
private:
	static std::uint32_t id;

	vluint64_t time = 0;

	VerilatedVcdC *vcd;

	block32_t out_block;

public:
	TbBlockFeeder() {
		Verilated::traceEverOn(true);
		vcd = new VerilatedVcdC;
		trace(vcd, 5);

		// !!Not thread safe.
		vcd->open((std::string("tb_BlockFeeder_wave_")
						.append(std::to_string(id))
				   + std::string(".vcd")).c_str());
		id++;
	}

	~TbBlockFeeder() {
		vcd->close();
		delete vcd;
	}

	void half_pulse() {
		Clk ^= 1;
		eval();
		vcd->dump(time);
		time++;
	}

	void pulse() {
		half_pulse();
		half_pulse();
	}

	void reset() {
		Clk = 1;
		Rst = 1;
		pulse();
		Rst = 0;
	}

	void write(std::istream& inStream) {
		uint32_t data_in = 0U;

		uint8_t data = (uint8_t)inStream.get();
		while (!inStream.eof()) {
			printf("Writing Char: 0x%02X.\n", data);

			// Write bit by bit.
			for (uint8_t j = 0U; j < 8U; j++) {
				Wdata = (data >> (7U - j)) & 0x01U;
				Wvalid = true;

				ASSERT(Rready, false);

				printf("Writing bit: %0u.\n", Wdata);

				WAIT(Wready, true, 512U, "Waiting for write ready signal.");

				pulse();

				// The data bit should have been stored into the buffer.
				data_in++;

				if (data_in == 512U) {
					// The buffer should be full so the write ready signal should
					// be off until the Next_block signals is pulsed.
					ASSERT(Wready, false);
					return;
				}
			}

			data = (uint8_t)inStream.get();
		}

		// Not necessary. These could be left high. The block shouldn't be affected.
		Wdata = 0;
		Wvalid = false;

		printf("Sending write end signal.\n");
		Wend = true;
		pulse();
		Wend = false;
	}

	const block32_t *getBlock() {
		static_assert(ARRAY_SIZE(out_block) == 16);
		for (uint8_t i = 0U; i < 16; i++) {
			Rid = i;
			Rvalid = true;

			// Wait for the block to accept the read.
			WAIT(Rready, true, 512U, "Waiting to read ready signal.");

			pulse();

			out_block[i] = Rword;
		}

		Rid = 0;
		Rvalid = false;

		return &out_block;
	}
};

std::uint32_t TbBlockFeeder::id = 0;

int main() {
	uint8_t passed = 0U;
	// Check that we can hold the count if all tests pass.
	static_assert((__typeof(passed)) ~0 >= ARRAY_SIZE(testDataSet));

	for (std::string input : testDataSet) {
		// Eventually we'll spawn a thread per test.
		std::istringstream dut_in_stream(input);
		TbBlockFeeder *dut = new TbBlockFeeder;
		std::istringstream in_stream(input);
		BlockFeeder *model = new BlockFeeder(in_stream);

		printf("Test input: \"%s\".\n", input.c_str());
		dut->reset();

		try {
			dut->write(dut_in_stream);
			model->next();

			const std::uint32_t *observed = *dut->getBlock();
			const std::uint32_t *expected = *to_block32_ptr(model->getBlock());

			while (model->hasNext()) {
				for (uint32_t i = 0U; i < 16; i++)
					ASSERT2(observed[i], expected[i], "Word");

				/* Signal the block feeder that the words have been processed
				 * and that it should move onto the next block.
				 */
				dut->Next_block = true;
				dut->pulse();
				dut->Next_block = false;

				model->next();
				dut->write(dut_in_stream);

				observed = *dut->getBlock();
				expected = *to_block32_ptr(model->getBlock());
			}

			ASSERT(dut->Term, true);

			for (uint32_t i = 0U; i < 16; i++)
				ASSERT2(observed[i], expected[i], "Word");

			// An extra one just to ease looking at the wave file.
			dut->pulse();

			passed++;
		} catch (ValueMismatchError valError) {
			printf("%s\n", valError.what());
		} catch (TimeOutError timeoutError) {
			printf("%s\n", timeoutError.what());
		}

		delete model;
		delete dut;
	}

	printf("\nResult: %u/%zu.\n", passed, ARRAY_SIZE(testDataSet));

	return !(passed == ARRAY_SIZE(testDataSet));
}
