/* Test-bench for the tb_RoundProcessor module in the md5hw project.
 * Copyright (C) 2022 Karmjit Mahil
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdint>
#include <cstdio>
#include <verilated.h>
#include <verilated_vcd_c.h>

#include "Vtb_RoundProcessor.h"

class RoundProcessor: public Vtb_RoundProcessor {
private:
	vluint64_t time = 0;

	VerilatedVcdC *vcd;

public:
	RoundProcessor() {
		Verilated::traceEverOn(true);
		vcd = new VerilatedVcdC;
		trace(vcd, 5);

		vcd->open("tb_RoundProcessor_wave.vcd");
	}

	~RoundProcessor() {
		vcd->close();
	}

	void half_pulse() {
		Clk ^= 1;
		eval();
		vcd->dump(time);
		time++;
	}

	void pulse() {
		half_pulse();
		half_pulse();
	}

	void reset() {
		Rst = 1;
		pulse();

		pulse();

		Rst = 0;
	}

	void trigger() {
		Trig = 1;
		half_pulse();

		Trig = 0;
		half_pulse();
	}

	int execCalc(const uint32_t data[16]) {
		while(Qvalid == 0) {
			half_pulse();

			if (time > 1000) {
				printf("Timeout failure!\n");
				return 1;
			}
		}

		return 0;
	}
};

int main() {
	int ret = 0;

	RoundProcessor *dut = new RoundProcessor();

	dut->reset();

	const uint32_t data[16] = {
		[0] = 0x80,
		[1] = 0x11,
	};

	dut->trigger();
	ret = dut->execCalc(data);
	dut->pulse();
	dut->pulse();
	if (ret == 0)
		printf("Successful!\n");
	else
		printf("Error!\n");


	delete dut;

	return ret;
}
