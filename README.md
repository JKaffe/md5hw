# md5hw

Hardware implementation of the md5 hashing algorithm. Do not use this for security sensitive use cases. Md5 has been shown to be vulnerable.

## Architecture.

The implementation is largely broken down into two main blocks: the Round Processor, and the Block Feeder.


The Block Feeder stores the data block to be processed. It also handles padding and appending the data length to the ending block. It can be asked to provide a data word by providing the word id (0 to 15, since each md5 block holds 16 words) and waiting for the read ready signal. Data is fetched from the system through an AXI DMA block (not done yet, but Xilinx'es AXI DMA will likely be used). Back pressure is handled by the write ready signal which is affected by the Round Processor having processed the block. To indicate the ending of the data, a write last signal is used (to ease compatibility with AXI).


The Round Processor requests appropriate data words from the Block Feeder and evaluates the md5 hash. A trigger signal must be provided to initiate the processing of a block. On completion of the 4 rounds a new trigger signal must be sent and a completion signal is outputted. The completion signal (not done yet) is used to direct the Block Feeder to fetch a new block.


To begin processing of a new data stream both units need resetting to clear out counters and initiate the registers according to the md5 algorithm.

## Status.

In development. To start out, the design is simplified and currently data is fed in 1 bit at a time. Performance in terms of clock cycle can be improved.

## Development setup.

Verilog is used as the HDL. Verilator is being used to quickly write cpp test-benches and test the hardware blocks. More thorough and accurate test-benches will later be produces in Verilog at which point the development setup will likely change to either integrate Xilinx Vivado's simulator or use the Icarus Verilog simulator.


Zedbord is the targeted FPGA board. Currently SymbiFlow's Xilinx FPGA support seems imcomplete and I couldn't find an easy and quick to use AXI DMA implementation, so Vivado will be used for it's block design capabilities, to constrain the design and produce the bit-streams.

## Licensing.

The HDL is licensed under the CERN OHL-S (strongly-reciprocal) v2 (unless otherwise noted), or (at your option) any later version.


The C++ code, including C++ code for the model and the verilator test-benches, is licensed under the GPL v3.0 (unless otherwise noted), or (at you option) any later version.
