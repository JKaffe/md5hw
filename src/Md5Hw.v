/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module Md5Hw
(
	input	wire		Clk, Rst,
			wire		Wvalid, Wend, Wdata,
	output	wire		Wready,
			wire		Qvalid,
			wire[31:0]	Q[3:0]
);

	wire[3:0] msg_word_idx;
	wire msg_word_idx_valid;
	wire[31:0] msg_word;
	wire msg_word_valid;

	wire processor_q_valid;
	wire data_end;

	assign Qvalid = next_state == END;

	reg[1:0] state;
	reg[1:0] next_state;

	always @(posedge Clk)
	begin
		if (Rst)
			state <= INIT;
		else begin
			state <= next_state;
		end
	end

	localparam INIT = 0;
	localparam TRIGGER = 1;
	localparam PROCESS = 2;
	localparam END = 3;

	reg trig;
	reg next_block;

	always @(state, Wready, Wvalid, Wend, processor_q_valid, data_end)
	begin
		assign trig = 0;
		assign next_block = 0;

		case (state)
			INIT:
			begin
				if (Wready & Wvalid | Wend) begin
					assign next_state = PROCESS;
				end else begin
					assign next_state = state;
				end
			end

			TRIGGER:
			begin
				assign trig = 1;
				assign next_state = PROCESS;
			end

			PROCESS:
			begin
				if (processor_q_valid)
					if (data_end)
						assign next_state = END;
					else begin
						trig = 1;
						next_block = 1;
						assign next_state = TRIGGER;
					end
				else begin
					assign next_state = state;
				end
			end

			END:
			begin
				assign next_state = state;
			end
		endcase
	end

	BlockFeeder DataFeeder(
		.Clk (Clk), .Rst (Rst),
		.Wvalid (Wvalid), .Wend (Wend),
		.Wdata (Wdata), .Wready (Wready),
		.Rid (msg_word_idx), .Rvalid (msg_word_idx_valid),
		.Rready (msg_word_valid), .Rword(msg_word),
		.Next_block (next_block),
		.Term (data_end)
	);

	RoundProcessor Processor(
		.Clk (Clk), .Rst (Rst),
		.Rword_ready (msg_word_valid), .Rword (msg_word),
		.Rword_valid (msg_word_idx_valid), .Rword_id (msg_word_idx),
		.Qvalid (processor_q_valid), .Q (Q),
		.Trig (trig)
	);

endmodule
