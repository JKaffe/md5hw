/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module BlockFeeder
(
	input	wire				Clk, Rst,
			wire				Wvalid, Wend, Wdata,
			wire[3:0]			Rid,
			wire				Rvalid,
			wire				Next_block,
	output	reg					Wready,
			reg					Rready,
			wire[WORD_SIZE-1:0]	Rword,
			wire				Term
);
	localparam BLOCK_SIZE			= 512;
	localparam WORD_SIZE			= 32;
	localparam MSG_COUNTER_SIZE		= 64;

	localparam INIT					= 0;
	localparam PADDING_INIT			= 1;
	localparam PADDING_CONTINUE		= 2;
	localparam END					= 3;

	reg[1:0] state;
	reg[1:0] next_state;

	// STM state transition.
	always @(posedge Clk)
	begin
		if (Rst)
			state <= INIT;
		else begin
			state <= next_state;
		end
	end

	reg[BLOCK_SIZE-1:0] data;
	reg[BLOCK_SIZE-1:0] data_next;
	reg data_force_write;

	reg[10:0] data_count;
	wire data_buff_full;

	assign data_buff_full = data_count[9];
	assign Wready = ~data_buff_full & (next_state == INIT);

	always @(posedge Clk)
	begin
		if (Rst | Next_block) begin
			Rready <= 0;
		end else begin
			if (data_buff_full | state == END) begin
				// This could be improved by looking at the read id and set
				// the ready whenever that word is stored in the data buffer.
				// I.e. after the first word is in the data buffer (the
				// data_count is at 32) the ready for word id 0 can be set to
				// 1. Since the first series of words is
				// seq(x) = seq(x - 1) + 1 , seq(0) = 0, 0 >= x > 16
				// the data requested by the RoundProcessor will be available
				// without stalling. The handshaking still takes 1 clock cycle
				// per request however this could be eliminated by presenting
				// a FIFO and just have 1 handshake per block).
				Rready <= 1;
			end
		end
	end

	wire[WORD_SIZE-1:0] words[BLOCK_SIZE/WORD_SIZE];

	// Word 0 is at the end of the buffer. The end of the buffer contains the
	// beginning of the data once the buffer is filled.
	genvar i;
	genvar j;
	generate
		for (i = 0; i < BLOCK_SIZE/WORD_SIZE; i = i + 1) begin
			// Endiannes swap.
			for (j = 0; j < WORD_SIZE/8; j = j + 1) begin
				assign words[BLOCK_SIZE/WORD_SIZE-1 - i][((j+1)*8)-1:j*8] =
					data[WORD_SIZE*(i+1)-1:WORD_SIZE*i][WORD_SIZE-(j*8)-1:WORD_SIZE-((j+1)*8)];
			end
		end
	endgenerate

	// This assumes that all words are ready on Rready.
	assign Rword =
		~(Rvalid & Rready)
			? 0
			: (state != END)
				? words[Rid]
				: (~(&Rid[3:1])) // Rid is not 0b1110 (14) or 0b1111 (15)
					/* The data shift register is not full but contains
					 * data up until word 2. So the 0th word is at Rid 2.
					 * Rid 14 and 15 are mapped to the message length.
					 */
					? words[Rid + 2]
					: (Rid[0]) // Rid is 15
						? msg_length[MSG_COUNTER_SIZE-1:MSG_COUNTER_SIZE-WORD_SIZE]
						: msg_length[MSG_COUNTER_SIZE-WORD_SIZE-1:0];

	always @(posedge Clk)
	begin
		if (Rst | Next_block) begin
			data_count <= 0;
		end else begin
			if (~data_buff_full) begin
				if (Wready & Wvalid | data_force_write) begin
					data <= data_next;
					data_count <= data_count + 1;
				end
			end
		end
	end

	always @(state, Wend, data, data_count, Wdata, data_buff_full)
	begin
		assign data_force_write = 0;

		case (state)
			INIT:
			begin
				if (Wend) begin
					if (data_count == 448) begin
						assign data_next = data;

						assign next_state = END;
					end else begin
						assign data_force_write = 1;
						assign data_next = {data[BLOCK_SIZE-1-1:0], 1'b1};

						assign next_state = PADDING_INIT;
					end
				end else begin
					assign data_next = {data[BLOCK_SIZE-1-1:0], Wdata};
				end
			end

			PADDING_INIT:
			begin
				assign data_force_write = 1;
				assign data_next = {data[BLOCK_SIZE-1-1:0], 1'b0};

				if (data_count == 448)
					assign next_state = END;
				else begin
					assign next_state = PADDING_CONTINUE;
				end
			end

			PADDING_CONTINUE:
			begin
				if (data_count == 448) begin
					assign data_next = data_next;

					assign next_state = END;
				end else begin
					assign data_force_write = 1;
					assign data_next = {data[BLOCK_SIZE-1-1:0], 1'b0};

					assign next_state = PADDING_CONTINUE;
				end
			end

			END:
			begin
				assign data_next = data;

				assign next_state = END;
			end
		endcase
	end

	// Indicates that there will be no more blocks. The data has terminated.
	assign Term = (state == END);

	// Message length counter.

	reg[MSG_COUNTER_SIZE-1:0] msg_length;

	always @(posedge Clk)
	begin
		if (Rst)
			msg_length <= 0;
		else begin
			if (Wready & next_state == INIT) begin
				msg_length <= msg_length + 1;
			end
		end
	end

endmodule
