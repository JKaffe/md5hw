/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module FunctionUnit
(
	input	wire[31:0]	B, C, D,
		wire[1:0]	Fsel,
	output	reg[31:0]	Q
);

	localparam F = 0;
	localparam G = 1;
	localparam H = 2;
	localparam I = 3;

	wire[31:0] Dn;
	assign Dn = ~D;

	wire[31:0] D_and_Bn;
	assign D_and_Bn = D & ~B;

	wire[31:0] B_xor_D;
	assign B_xor_D = B & Dn | D_and_Bn;

	wire[31:0] Qf;
	assign Qf = B & C | D_and_Bn;

	wire[31:0] Qg;
	assign Qg = B & D | C & Dn;

	wire[31:0] Qh;
	assign Qh = B_xor_D ^ C;

	wire[31:0] Qi;
	assign Qi = C ^ (~D_and_Bn);

	always @(Fsel or Qf or Qg or Qh or Qi)
	begin
		case (Fsel)
			F: Q = Qf;
			G: Q = Qg;
			H: Q = Qh;
			I: Q = Qi;
		endcase;
	end
endmodule;
