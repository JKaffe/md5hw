/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module RotateLeftUnit
(
	input	wire[31:0]	D,
		wire[4:0]	S,
	output	wire[31:0]	Q
);
	wire[31:0] rotates[0:31];

	genvar i;

	assign rotates[0] = D;

	generate
		for (i = 1; i < 32; i = i + 1) begin
			assign rotates[i] = {D[31-i:0], D[31:31-i+1]};
		end
	endgenerate;

	assign Q = rotates[S];

endmodule;
