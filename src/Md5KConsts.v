/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module Md5KConsts
(
	input	wire[5:0]	Select,
	output	wire[31:0]	Q
);
	wire[31:0] K[0:63];

	assign K[0] = 32'hD76AA478;
	assign K[1] = 32'hE8C7B756;
	assign K[2] = 32'h242070DB;
	assign K[3] = 32'hC1BDCEEE;

	assign K[4] = 32'hF57C0FAF;
	assign K[5] = 32'h4787C62A;
	assign K[6] = 32'hA8304613;
	assign K[7] = 32'hFD469501;

	assign K[8] = 32'h698098D8;
	assign K[9] = 32'h8B44F7AF;
	assign K[10] = 32'hFFFF5BB1;
	assign K[11] = 32'h895CD7BE;

	assign K[12] = 32'h6B901122;
	assign K[13] = 32'hFD987193;
	assign K[14] = 32'hA679438E;
	assign K[15] = 32'h49B40821;

	assign K[16] = 32'hF61E2562;
	assign K[17] = 32'hC040B340;
	assign K[18] = 32'h265E5A51;
	assign K[19] = 32'hE9B6C7AA;

	assign K[20] = 32'hD62F105D;
	assign K[21] = 32'h02441453;
	assign K[22] = 32'hD8A1E681;
	assign K[23] = 32'hE7D3FBC8;

	assign K[24] = 32'h21E1CDE6;
	assign K[25] = 32'hC33707D6;
	assign K[26] = 32'hF4D50D87;
	assign K[27] = 32'h455A14ED;

	assign K[28] = 32'hA9E3E905;
	assign K[29] = 32'hFCEFA3F8;
	assign K[30] = 32'h676F02D9;
	assign K[31] = 32'h8D2A4C8A;

	assign K[32] = 32'hFFFA3942;
	assign K[33] = 32'h8771F681;
	assign K[34] = 32'h6D9D6122;
	assign K[35] = 32'hFDE5380C;

	assign K[36] = 32'hA4BEEA44;
	assign K[37] = 32'h4BDECFA9;
	assign K[38] = 32'hF6BB4B60;
	assign K[39] = 32'hBEBFBC70;

	assign K[40] = 32'h289B7EC6;
	assign K[41] = 32'hEAA127FA;
	assign K[42] = 32'hD4EF3085;
	assign K[43] = 32'h04881D05;

	assign K[44] = 32'hD9D4D039;
	assign K[45] = 32'hE6DB99E5;
	assign K[46] = 32'h1FA27CF8;
	assign K[47] = 32'hC4AC5665;

	assign K[48] = 32'hF4292244;
	assign K[49] = 32'h432AFF97;
	assign K[50] = 32'hAB9423A7;
	assign K[51] = 32'hFC93A039;

	assign K[52] = 32'h655B59C3;
	assign K[53] = 32'h8F0CCC92;
	assign K[54] = 32'hFFEFF47D;
	assign K[55] = 32'h85845DD1;

	assign K[56] = 32'h6FA87E4F;
	assign K[57] = 32'hFE2CE6E0;
	assign K[58] = 32'hA3014314;
	assign K[59] = 32'h4E0811A1;

	assign K[60] = 32'hF7537E82;
	assign K[61] = 32'hBD3AF235;
	assign K[62] = 32'h2AD7D2BB;
	assign K[63] = 32'hEB86D391;

	assign Q = K[Select];

endmodule;
