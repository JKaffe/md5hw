/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module tb_RoundProcessor
(
	input	wire		Clk, Rst,
			wire		Trig,
	output	wire		Qvalid,
			wire[31:0]	Q[3:0]
);

	wire Rword_valid, Rword_ready;
	wire[3:0] Rword_id;
	wire[31:0] Rword;

	RoundProcessor Processor(
		.Clk (Clk), .Rst (Rst),
		.Trig (Trig),
		.Rword_ready (Rword_ready),
		.Rword (Rword),
		.Rword_id (Rword_id),
		.Rword_valid (Rword_valid),
		.Qvalid (Qvalid),
		.Q (Q)
	);

	MockBlockFeeder DataFeeder(
		.Clk (Clk),
		.Rst (Rst),
		.Rword_id (Rword_id),
		.Rword_valid (Rword_valid),
		.Rword_ready (Rword_ready),
		.Rword (Rword)
	);

endmodule;
