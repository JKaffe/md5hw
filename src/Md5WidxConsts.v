/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module Md5WidxConsts
(
	input	wire[5:0]	Select,
	output	wire[3:0]	Q
);
	wire[3:0] Widx[0:63];

	assign Widx[0] = 0;
	assign Widx[1] = 1;
	assign Widx[2] = 2;
	assign Widx[3] = 3;
	assign Widx[4] = 4;
	assign Widx[5] = 5;
	assign Widx[6] = 6;
	assign Widx[7] = 7;
	assign Widx[8] = 8;
	assign Widx[9] = 9;
	assign Widx[10] = 10;
	assign Widx[11] = 11;
	assign Widx[12] = 12;
	assign Widx[13] = 13;
	assign Widx[14] = 14;
	assign Widx[15] = 15;

	assign Widx[16] = 1;
	assign Widx[17] = 6;
	assign Widx[18] = 11;
	assign Widx[19] = 0;
	assign Widx[20] = 5;
	assign Widx[21] = 10;
	assign Widx[22] = 15;
	assign Widx[23] = 4;
	assign Widx[24] = 9;
	assign Widx[25] = 14;
	assign Widx[26] = 3;
	assign Widx[27] = 8;
	assign Widx[28] = 13;
	assign Widx[29] = 2;
	assign Widx[30] = 7;
	assign Widx[31] = 12;

	assign Widx[32] = 5;
	assign Widx[33] = 8;
	assign Widx[34] = 11;
	assign Widx[35] = 14;
	assign Widx[36] = 1;
	assign Widx[37] = 4;
	assign Widx[38] = 7;
	assign Widx[39] = 10;
	assign Widx[40] = 13;
	assign Widx[41] = 0;
	assign Widx[42] = 3;
	assign Widx[43] = 6;
	assign Widx[44] = 9;
	assign Widx[45] = 12;
	assign Widx[46] = 15;
	assign Widx[47] = 2;

	assign Widx[48] = 0;
	assign Widx[49] = 7;
	assign Widx[50] = 14;
	assign Widx[51] = 5;
	assign Widx[52] = 12;
	assign Widx[53] = 3;
	assign Widx[54] = 10;
	assign Widx[55] = 1;
	assign Widx[56] = 8;
	assign Widx[57] = 15;
	assign Widx[58] = 6;
	assign Widx[59] = 13;
	assign Widx[60] = 4;
	assign Widx[61] = 11;
	assign Widx[62] = 2;
	assign Widx[63] = 9;

	assign Q = Widx[Select];

endmodule;
