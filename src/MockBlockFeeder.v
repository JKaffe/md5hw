/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module MockBlockFeeder
(
	input	wire			Clk, Rst,
			wire[3:0]		Rword_id,
			wire			Rword_valid,
	output	reg				Rword_ready,
			reg[31:0]		Rword
);

	reg[3:0] word_id;

	always @(posedge Clk) begin
		if (Rst) begin
			Rword <= 32'b0;
			word_id <= 4'b0;
		end else begin
			if (Rword_valid) begin
				// Simulating a block for empty data. I.e. Padding only.
				if (Rword_id == 4'h0)
					Rword <= 32'h80;
				else begin
					Rword <= 32'b0;
				end

				word_id <= Rword_id;
			end
		end
	end

	assign Rword_ready = ~Rst & (word_id == Rword_id);

endmodule;
