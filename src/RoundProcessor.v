/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module RoundProcessor
(
	input	wire		Clk, Rst,
			wire		Trig,
			wire		Rword_ready,
			reg[31:0]	Rword,
	output	wire[3:0]	Rword_id,
			wire		Rword_valid,
			wire		Qvalid,
			reg[31:0]	Q[3:0]
);

	wire CalcComplete;
	assign CalcComplete = Op_count[6] & Op_count[0];

	wire En;
	assign En = ~(CalcComplete) & (Rword_valid & Rword_ready);

	reg[6:0] Op_count;

	always @(posedge Clk)
	begin
		if (Rst | Trig)
			Op_count <= 7'b0;
		else begin
			if (En) begin
				Op_count <= Op_count + 1;
			end
		end
	end

	reg[31:0] A, B, C, D;
	reg[31:0] Aold, Bold, Cold, Dold;
	wire[31:0] Anext, Bnext, Cnext, Dnext;

	always @(posedge Clk)
	begin
		if (Rst) begin
			A <= 32'h67452301;
			B <= 32'hEFCDAB89;
			C <= 32'h98BADCFE;
			D <= 32'h10325476;

			Aold <= 32'h67452301;
			Bold <= 32'hEFCDAB89;
			Cold <= 32'h98BADCFE;
			Dold <= 32'h10325476;
		end else begin
			if (En) begin
				A <= Anext;
				B <= Bnext;
				C <= Cnext;
				D <= Dnext;
			end
		end
	end

	always @(posedge Clk)
	begin
		if (Trig) begin
			Aold <= A;
			Bold <= B;
			Cold <= C;
			Dold <= D;
		end
	end

	Md5WidxConsts WordIndecies (.Select (Op_count[5:0]), .Q (Rword_id));
	assign Rword_valid = 1'b1;

	wire[31:0] Qf;
	FunctionUnit FUnit(.B (B), .C (C), .D (D), .Fsel (Op_count[5:4]), .Q (Qf));

	wire[31:0] K;
	Md5KConsts KConsts(.Select (Op_count[5:0]), .Q (K));

	wire[4:0] S;
	Md5SConsts SConsts(.Select ({Op_count[5:4], Op_count[1:0]}), .Q (S));

	wire[31:0] pre_rotate_result;
	assign pre_rotate_result = A + Qf + Rword + K;

	wire[31:0] rotated_result;
	RotateLeftUnit RRLUnit(.D (pre_rotate_result), .S (S), .Q (rotated_result));

	assign Anext = (Op_count[6]) ? A + Aold : D;
	assign Bnext = (Op_count[6]) ? B + Bold : rotated_result + B;
	assign Cnext = (Op_count[6]) ? C + Cold : B;
	assign Dnext = (Op_count[6]) ? D + Dold : C;

	assign Qvalid = CalcComplete;

	assign Q[0] = (CalcComplete) ? A : 32'b0;
	assign Q[1] = (CalcComplete) ? B : 32'b0;
	assign Q[2] = (CalcComplete) ? C : 32'b0;
	assign Q[3] = (CalcComplete) ? D : 32'b0;

endmodule;
