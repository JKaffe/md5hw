/* Copyright Karmjit Mahil 2022.
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL-S v2.
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 * Source location: https://github.com/JKaffe/md5hw
 *
 * As per CERN-OHL-S v2 section 4, should You produce hardware based on this
 * source, You must where practicable maintain the Source Location visible on
 * the external case of the Gizmo or other products you make using this source.
 */

module Md5SConsts
(
	input	wire[3:0]	Select,
	output	wire[4:0]	Q
);

	wire[4:0] S[0:15];

	assign S[0] = 5'd7;
	assign S[1] = 5'd12;
	assign S[2] = 5'd17;
	assign S[3] = 5'd22;

	assign S[4] = 5'd5;
	assign S[5] = 5'd9;
	assign S[6] = 5'd14;
	assign S[7] = 5'd20;

	assign S[8] = 5'd4;
	assign S[9] = 5'd11;
	assign S[10] = 5'd16;
	assign S[11] = 5'd23;

	assign S[12] = 5'd6;
	assign S[13] = 5'd10;
	assign S[14] = 5'd15;
	assign S[15] = 5'd21;

	assign Q = S[Select];
endmodule;
